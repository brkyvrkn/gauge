//
//  GaugeModel+Path.swift
//  GaugeExample
//
//  Created by Berkay Vurkan on 21.09.2019.
//  Copyright © 2019 asgaard. All rights reserved.
//

import Foundation
import UIKit

extension GaugeModel {

    func gaugePath() -> UIBezierPath {
        let center_X = self.bounds.midX
        let center_Y = self.bounds.maxY - (self.lineWidth / 2)
        let center = CGPoint(x: center_X, y: center_Y)

        if self.ratio < 0 {
            self.ratio = 0
        } else if self.ratio > 1 {
            self.ratio = 1
        }

        var startAngle = CGFloat(Double.pi)
        var endAngle = CGFloat(Float.pi + (Float.pi * self.ratio))
        if !isClockwise {
            startAngle = CGFloat(0)
            endAngle = CGFloat(2 * Float.pi - (Float.pi * self.ratio))
        }

        self.radii = self.findOptimalRadius(in: self.bounds, lineWidth: self.lineWidth)
        let path = UIBezierPath(arcCenter: center, radius: self.radii, startAngle: startAngle, endAngle: endAngle, clockwise: isClockwise)

        return path
    }

    func gaugeShapeLayer(path: UIBezierPath, isRounded: Bool) -> CAShapeLayer {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = self.strokeColor.cgColor
        shapeLayer.fillColor = self.fillColor.cgColor
        shapeLayer.name = self.name + "_GaugeShapeLayer"
        shapeLayer.lineWidth = self.lineWidth
        shapeLayer.lineCap = isRounded ? .round : .square

        return shapeLayer
    }


    // MARK: - Helpers

    private func findOptimalRadius(in rect: CGRect, lineWidth: CGFloat) -> CGFloat {
        let width = rect.size.width
        let height = rect.size.height
        let doubledRect = CGRect(origin: rect.origin, size: CGSize(width: width, height: height * 2))

        if width > doubledRect.size.height {
            return (doubledRect.size.height / 2) - lineWidth
        }

        return (width - lineWidth) / 2
    }

    private func findMaximumSquare(in rect: CGRect) -> CGRect {
        // Gauge will locate inside of the rectangle
        // which can be fitted into the given rectangle
        let width = rect.size.width
        let height = rect.size.height
        let side = width < height ? width : height

        return CGRect(x: 0, y: 0, width: side, height: side)
    }
}
