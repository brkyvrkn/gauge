//
//  InstantiatableView.swift
//  GaugeExample
//
//  Created by Berkay Vurkan on 24.09.2019
//  Copyright © 2019 asgaard. All rights reserved.
//

import Foundation
import UIKit

public enum UIViewError: Error {
    
    case nibInitialize(message: String)
    case alreadyInitialized(message: String)
}

public protocol InstantiatableUIView: UIView {
    
    func accessViewFromNib() -> UIView?
    
    /// Call configureView function at the beginning of this scope,
    /// but remember that the function is throwable and you should handle
    /// any nib initializer error.
    ///
    /// Then other operations will follow through forward.
    ///
    /// - Returns: Void
    func commonInit() -> Void
}

public extension InstantiatableUIView {
    
    /// Get UIView object that is designed in .xib file
    /// Constraints:
    ///     - The name of Xib and Swift files must be the same
    ///     - Xib and Swift files must be the part of the same bundle
    ///
    /// - Returns: Optional UIView designed in the .xib
    func accessViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        
        return view
    }
    
    /// View initialization method, have to be called in the common init method
    ///
    /// - Parameter isAutoLayoutActive: Determine the view's side constraints
    /// - Throws: UIView initialization errors, declared as UIViewError
    func configureView(isAutoLayoutActive: Bool = true) throws -> Void {
        let className = String(describing: Self.self)
        
        guard subviews.count == 0 else {
            let errMessage = NSLocalizedString("Subview(s) is already created in \(className)", comment: "").capitalized
            throw UIViewError.alreadyInitialized(message: errMessage)
        }
        
        guard let view = accessViewFromNib() else {
            let errMessage = NSLocalizedString("UIView cannot be accessed via Nib <\(className)>", comment: "").capitalized
            throw UIViewError.nibInitialize(message: errMessage)
        }
        
        setDefaultConstraints(in: view, isAutoLayoutActive)
    }
    
    private func setDefaultConstraints(in view: UIView, _ autoLayout: Bool = true) {
        if autoLayout {
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.translatesAutoresizingMaskIntoConstraints = true
            addSubview(view)
        } else {
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubview(view)
            let vertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: .alignAllLastBaseline, metrics: nil, views: ["view": view])
            let horizontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: .alignAllLastBaseline, metrics: nil, views: ["view": view])
            addConstraints(vertical + horizontal)
        }
    }
}
