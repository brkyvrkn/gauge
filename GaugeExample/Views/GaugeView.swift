//
//  GaugeView.swift
//  Component Samples
//
//  Created by Berkay Vurkan on 16.09.2019
//  Copyright © 2019 Temp. All rights reserved.
//

import UIKit

class GaugeView: UIView, InstantiatableUIView {

    // MARK: - Outlets
    @IBOutlet var view: UIView!
    @IBOutlet var gaugeZone: UIView!
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!

    //MARK:- Properties
    private var model: GaugeModel!

    override init(frame: CGRect) {
        super.init(frame: frame)

        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        commonInit()
    }

    func commonInit() {
        try? configureView()

        setUI()
    }

    func setUI() {
        self.upperLabel.backgroundColor = .systemBackground
        self.bottomLabel.backgroundColor = .systemBackground
        self.view.backgroundColor = .systemBackground
        self.gaugeZone.backgroundColor = .clear
    }

    func draw(gauge: GaugeModel, animated: Bool = false) {
        self.model = gauge
        self.model.bounds = self.gaugeZone.bounds
        drawGauge(animated: animated)
    }

    func drawGauge(animated: Bool) {
        guard model != nil else { return }

        // Parameters
        self.gaugeZone.backgroundColor = model.fillColor
        self.upperLabel.text = self.model.title
        self.bottomLabel.text = self.model.footer

        let path = self.model.gaugePath()
        let animation = drawingAnimation()
        let outerLayer = self.model.gaugeShapeLayer(path: path, isRounded: true)

        if animated {
            outerLayer.add(animation, forKey: "drawingLineAnimation")
        }

        self.gaugeZone.layer.insertSublayer(outerLayer, at: 0)
    }

    func removeGauge() {
        guard let subLayers = self.gaugeZone.layer.sublayers else { return }
        subLayers.forEach({ (layer) in
            if layer.name == self.model.name + "_GaugeShapeLayer" {
                layer.removeAnimation(forKey: "drawingLineAnimation")
                layer.removeFromSuperlayer()
            }
        })
        self.gaugeZone.backgroundColor = .clear
        self.upperLabel.text = "-"
        self.bottomLabel.text = "-"
    }

    func drawingAnimation() -> CABasicAnimation {
        let anim = CABasicAnimation(keyPath: "strokeEnd")
        anim.duration = 0.5
        anim.fromValue = 0.0
        anim.toValue = 1.0

        return anim
    }
}
