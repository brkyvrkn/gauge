//
//  ViewController.swift
//  GaugeExample
//
//  Created by Berkay Vurkan on 21.09.2019.
//  Copyright © 2019 asgaard. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var drawButton: UIButton!
    @IBOutlet weak var gauge: GaugeView!

    // MARK: - Properties
    var isGaugeDrawn = false

    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }

    func initUI() {
        self.view.backgroundColor = .systemBackground

        self.drawButton.titleLabel?.font = .systemFont(ofSize: 14)
        self.drawButton.layer.cornerRadius = 4
        self.drawButton.clipsToBounds = true
    }

    // MARK: - Actions
    @IBAction func drawButtonTapped(_ sender: Any) {
        let model = GaugeModel()
        let ratio = 0.65
        model.title = "Sample Gauge"
        model.footer = String(format: "%.0f", ratio * 100) + "%"
        model.ratio = Float(ratio)
        if !self.isGaugeDrawn {
            self.gauge.draw(gauge: model, animated: true)
            self.drawButton.setTitle("Remove Gauge", for: .normal)
        } else {
            self.gauge.removeGauge()
            self.drawButton.setTitle("Draw Gauge", for: .normal)
        }

        self.isGaugeDrawn = !self.isGaugeDrawn
    }
}

