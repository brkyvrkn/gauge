//
//  GaugeModel.swift
//  Component Samples
//
//  Created by Berkay Vurkan on 16.09.2019
//  Copyright © 2019 Temp. All rights reserved.
//

import Foundation
import UIKit

public class GaugeModel {

    // Identifier
    var name: String = "Default"

    // Features
    var title: String = ""
    var footer: String = ""

    // Frame related
    var bounds: CGRect = .zero
    var radii: CGFloat = 10.0

    // UI related
    var strokeColor: UIColor = .systemGray
    var fillColor: UIColor = .systemBackground
    var lineWidth: CGFloat = 2.0
    var ratio: Float = 1.0

    // Flags
    var isClockwise: Bool = true
}
